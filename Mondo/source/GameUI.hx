package;

import flixel.addons.display.FlxExtendedSprite;

class GameUI extends FlxExtendedSprite
{
    static inline var ASSETS_IMAGES_PNG = "assets/images/background_ui.png";

    public function new(x:Float = 0, y:Float = 0)
    {
        super(x, y);
        loadGraphic(ASSETS_IMAGES_PNG);
    }
}
