package;

import flixel.FlxState;

class PlayState extends FlxState
{
    private var board:Board;
    private var gameUI:GameUI;

	override public function create()
	{
		super.create();
        gameUI = new GameUI();
        board = new Board();
        add(gameUI);
        add(board);
	}

	override public function update(elapsed:Float)
	{
		super.update(elapsed);
	}    
}