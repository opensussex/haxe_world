package;

class MapTileFactory
{
    public static function create(mapTile:String):MapTile
    {
        switch(mapTile) {
            case 'Sea':
                return new SeaTile();
            case 'Sand' :
                return new SandTile();
            case 'Grass' :
                return new GrassTile();
            case 'Dirt' : 
                return new DirtTile();
            case 'Snow' :
                return new SnowTile();
            case 'Rock' :
                return new RockTile();
            default:
                return new SeaTile();
      }
  }
}
