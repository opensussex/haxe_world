package;

import flixel.FlxSprite;
import flixel.group.FlxSpriteGroup;
import flixel.addons.display.FlxExtendedSprite;


typedef Neighbours = {
    var above:Null<Array<Array<Int>>>;
    var side:Null<Array<Array<Int>>>;
    var below:Null<Array<Array<Int>>>;
};

class MapTileGroup extends FlxSpriteGroup
{
    private var mapTile:MapTile;
    private var mapTileSelect:MapTileSelect;
    private var boardLocation:Array<Int>;
    private var neighbours:Neighbours;
    public function new(x:Float = 0, y:Float = 0, tile:String = 'Sea')
    {
        super(x, y);
        mapTile = MapTileFactory.create(tile);
        mapTileSelect = new MapTileSelect();
        mapTileSelect.alpha = 0;
        boardLocation = new Array<Int>();
        add(mapTile);
        add(mapTileSelect);
    }

    public function swapTile(tile:String)
    {
        remove(mapTile);
        remove(mapTileSelect);
        mapTile = MapTileFactory.create(tile);
        add(mapTile);
        add(mapTileSelect);
    }

    public function toggleSelect()
    {
        if (mapTileSelect.alpha ==1) {
            hideSelect();
        } else {
            showSelect();
        }
    }

    public function hideSelect()
    {
        mapTileSelect.alpha = 0;
    }

    public function showSelect()
    {
        mapTileSelect.alpha = 1;
    }

    public function placeOnBoard(sY:Int, sX:Int)
    {
        boardLocation.push(sY);
        boardLocation.push(sX);
        setNeighbours(); 
    }

    public function getBoardLocation():Array<Int>
    {
        return boardLocation;
    }

    public function getNeighbours():Neighbours
    {
        return neighbours;
    }

    private function setNeighbours()
    {
        var above:Array<Array<Int>> = [];
        var side:Array<Array<Int>> = [];
        var below:Array<Array<Int>> = [];
        if (boardLocation != null) {
            if (boardLocation[0] == 0 && boardLocation[1] == 0) { // top left corner
                below.push([boardLocation[0] + 1, boardLocation[1]]);
                below.push([boardLocation[0] + 1, boardLocation[1] + 1]);
                side.push([boardLocation[0], boardLocation[1] + 1]);
            } else if (boardLocation[0] == 0 && boardLocation[1] == 14) { // top right corner
                side.push([boardLocation[0], boardLocation[1] -1]);
                below.push([boardLocation[0] + 1, boardLocation[1]]);
            } else if(boardLocation[0] == 14 && boardLocation[1] == 0) { // bottom left corner
                side.push([boardLocation[0], boardLocation[1]+1]);
                above.push([boardLocation[0] - 1, boardLocation[1]]);
                above.push([boardLocation[0] - 1, boardLocation[1] + 1]);
            }else if (boardLocation[0] == 14 && boardLocation[1] == 14) { // bottom right corner
                side.push([boardLocation[0], boardLocation[1] - 1]);
                above.push([boardLocation[0] - 1, boardLocation[1] -1]);
            }else if (boardLocation[0] == 0 && boardLocation[1] != 0 && boardLocation[1] != 14) { // top row
                side.push([boardLocation[0], boardLocation[1] - 1]);
                side.push([boardLocation[0], boardLocation[1] + 1]);
                below.push([boardLocation[0] + 1, boardLocation[1] - 1]);
                below.push([boardLocation[0] + 1, boardLocation[1] + 1]);
            }else if (boardLocation[0] == 14 && boardLocation[1] != 0 && boardLocation[1] !=14) { // bottom row
                side.push([boardLocation[0], boardLocation[1] - 1]);
                side.push([boardLocation[0], boardLocation[1] + 1]);
                above.push([boardLocation[0] - 1, boardLocation[1]]);
                above.push([boardLocation[0] - 1, boardLocation[1] + 1]);
            } else if (boardLocation[0] != 0 && boardLocation[0] != 14 && boardLocation[1] != 14 && boardLocation[1] == 0) { // left edge 
                side.push([boardLocation[0], boardLocation[1] + 1]);
                if (boardLocation[0] % 2 == 0) {
                    below.push([boardLocation[0] + 1, boardLocation[1]]);
                    below.push([boardLocation[0] + 1, boardLocation[1] + 1]);
                    above.push([boardLocation[0] - 1, boardLocation[1]]);
                    above.push([boardLocation[0] - 1, boardLocation[1] + 1]);
                } else {
                    below.push([boardLocation[0] + 1, boardLocation[1]]);
                    above.push([boardLocation[0] - 1, boardLocation[1]]);
                }
            } else if (boardLocation[0] != 0 && boardLocation[0] != 14 && boardLocation[1] != 0 && boardLocation[1] == 14) { // right edge
                side.push([boardLocation[0], boardLocation[1] - 1]);
                if (boardLocation[0] % 2 == 0) {
                    below.push([boardLocation[0] + 1, boardLocation[1]]);
                    above.push([boardLocation[0] - 1, boardLocation[1]]);
                } else {
                    below.push([boardLocation[0] + 1, boardLocation[1]]);
                    below.push([boardLocation[0] + 1, boardLocation[1] - 1]);
                    above.push([boardLocation[0] - 1, boardLocation[1]]);
                    above.push([boardLocation[0] - 1, boardLocation[1] - 1]);
                }
            } else { // the rest
                side.push([boardLocation[0], boardLocation[1] - 1]);
                side.push([boardLocation[0], boardLocation[1] + 1]);
                if (boardLocation[0] % 2 == 0) {
                    below.push([boardLocation[0] + 1, boardLocation[1] + 1]);
                    below.push([boardLocation[0] + 1, boardLocation[1]]);
                    above.push([boardLocation[0] - 1, boardLocation[1] + 1]);
                    above.push([boardLocation[0] - 1, boardLocation[1]]);
                } else {
                    below.push([boardLocation[0] + 1, boardLocation[1] - 1]);
                    below.push([boardLocation[0] + 1, boardLocation[1]]);
                    above.push([boardLocation[0] - 1, boardLocation[1] - 1]);
                    above.push([boardLocation[0] - 1, boardLocation[1]]);
                }
            }
        }

        neighbours = {
            above: above,
            side: side,
            below: below
        }
    }
}
