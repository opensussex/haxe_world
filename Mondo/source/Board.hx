package;

import flixel.group.FlxSpriteGroup;
import flixel.input.mouse.FlxMouseEventManager;
import flixel.input.mouse.FlxMouseButton.FlxMouseButtonID;

class Board extends FlxSpriteGroup
{

    private var selectedMapTile: Null<MapTileGroup>;
    private var board:Array<Array<MapTileGroup>>;

    public function new(x:Float = 0, y:Float = 0)
    {
        super(x, y);
        FlxMouseEventManager.init();
        board = new Array();
       
        setUp();
        randomStart();
    }

    function setUp()
    {
        var height:Int = 58;
        var width:Int = 50;
        var xStart:Int = 20;
        var yStart:Int = 80;
        var xOffset:Int = 0;
        var yOffset:Int = 0;
        var pixelPerfect = true;
        yOffset = Std.int(height * (3/4));
        var yPos:Int = 0;
        
        for (y in 0...15) {
            var boardRow = new Array<MapTileGroup>();
            if ((y % 2) == 0) {
                xOffset = Std.int(width / 2);
            } else {
                xOffset = 0;
            }

            if(y == 0) {
                yPos = 0;
            } else {
                yPos = yPos + yOffset;
            }
            for (x in 0...15) {
                var tile = new MapTileGroup((width*x + xOffset) + xStart, yPos + yStart);
                tile.placeOnBoard(y, x);
                boardRow.push(tile);
                add(tile);
                FlxMouseEventManager.add(
                    tile, 
                    mousePressedCallback, 
                    mouseReleasedCallback, 
                    null, 
                    null, 
                    false, 
                    true, 
                    pixelPerfect, 
                    [FlxMouseButtonID.LEFT, FlxMouseButtonID.RIGHT]
                );
            }
            board.push(boardRow);
        }        
    }

    function mousePressedCallback(tile:MapTileGroup)
    {
        trace('Pressed');
        if(selectedMapTile != null) {
            selectedMapTile.toggleSelect();
        }
        selectedMapTile = tile;
        tile.toggleSelect();
        trace(tile.getBoardLocation());
        trace(tile.getNeighbours());
    }

    function mouseReleasedCallback(tile:MapTileGroup)
    {
        trace('Released');
    }

    function randomStart()
    {
        var startX = Std.random(15);
        var startY = Std.random(15);
        //startX = 14;
        //startY = 0;
        var startTile = board[startY][startX];
        var startTileNeighbours = startTile.getNeighbours();

        swapTile(board[startY][startX]);
        trace(startY, startX);
        trace(startTileNeighbours);
        for(side in startTileNeighbours.side) {
            swapTile(board[side[0]][side[1]]);
        }
        for(above in startTileNeighbours.above) {
            swapTile(board[above[0]][above[1]]);
        }

        for(below in startTileNeighbours.below) {
            swapTile(board[below[0]][below[1]]);
        }
        
    }

    private function swapTile(tileToSwap:MapTileGroup)
    {
        tileToSwap.swapTile(getRandomTileType());
    }

    private function getRandomTileType() : String
    {
        
        var tileTypes:Array<String> = ['Snow','Sand','Rock','Dirt', 'Grass'];
        trace(Std.random(tileTypes.length));
        return tileTypes[Std.random(tileTypes.length)];
    }
}
