package;

import flixel.FlxSprite;
import flixel.addons.display.FlxExtendedSprite;

class MapTileSelect extends FlxExtendedSprite
{
    static inline var ASSETS_PNG = "assets/images/inner_outline.png";

    public function new(x:Float = 0, y:Float = 0)
    {
        super(x, y);
        loadGraphic(ASSETS_PNG);
    }
}
