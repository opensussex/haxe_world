package;

class RockTile extends MapTile
{
    static inline var ASSETS_IMAGES_PNG = "assets/images/rock_01.png";

    public function new(x:Float = 0, y:Float = 0)
    {
        super(x, y);
        loadGraphic(ASSETS_IMAGES_PNG);
    }
}
